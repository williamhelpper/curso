
const fs = require('fs');

// PARSER
const bodyParser = require('body-parser');
// create application/json parser
const jsonParser = bodyParser.json()


// LEITOR DE ARQUIVOS [[JSON]]
/**
 * Verifica e adiciona conforme necessidade `.json` ao final do caminho do arquivo
 * @param {string} file 
 */
function prepareFile(file) {
	if (!String(file).endsWith('.json')) file = `${file}.json`;
	return file;
}
/**
 * readFile
 * Lê o data de um arquivo json
 * @param file string 
 * @return object json 
 * ```ts 
 * readFile('user')
 * ```
 */
function readFile(file) {
	file = prepareFile(file);
	return JSON.parse(fs.readFileSync(file, 'utf8'))
}
/**
 * writeFile
 * Insere o data de um arquivo json
 * @param file string 
 * @param content obj 
 * ```ts 
 * writeFile('user', {"name":"john"})
 * ```
 */
function writeFile(file, content) {
	file = prepareFile(file);
	function err(err) { if (err) { return { "response": "Failed" } } return { "response": "Success" } }
	fs.writeFileSync(file, JSON.stringify(content), 'utf8', err)
	const obj = readFile(file);
	const length = (obj && obj.length) ? obj.length : 0;
	return ({ "writeFile": "Success", "data": obj.splice(0, 10), "totalRecords": length })
}
/**
 * Retorna os itens do array, com paginação se necessário
 * ```ts
 * getItems('user', { "page": 1, "limit": 10 })
 * ```
 * @param {string} file 
 * @param {obj} params
 */
function getItems(file, params = {}) {
	const obj = readFile(file);
	let data;
	// let response = {};
	// let response = {
	// 	totalRecords: obj.length,
	// };
	if (params.page && params.limit) {
		const reducedObj = [];

		for (let i = (params.limit * (params.page - 1)); i < params.page * params.limit; i++) {
			if (obj[i]) {
				reducedObj.push(obj[i]);
			}
		}

		data = reducedObj;
	} else {
		data = obj;
	}

	// return response;
	// return writeFile(file, response)
	return ({ "readFile": "Success", "data": data.splice(0, 10), "totalRecords": (obj.length) ? obj.length : null })
}
function filterItems(file, params = {}) {
	const obj = readFile(file);
	let response = { data: {} };

	response.data = obj.filter(item => {
		const keys = Object.keys(params)
		for (let key of keys) {
			if (String(item[key]).toLowerCase().indexOf(String(params[key]).toLowerCase()) != -1)
				return item;
		}
	})

	return response;
}
/**
 * Insere um novo item no array
 * @param {string} file 
 * @param {obj} content 
 * ```ts
 * createItem('user', { "name":"john" });
 * ```
 */
function createItem(file, content) {
	const obj = readFile(file);
	if (content && content.length && content.length > 0) {
		content.map(item => {
			item.id = Math.random().toString(36).substring(5);
			obj.push(item)
		})
	} else {
		content.id = Math.random().toString(36).substring(5);
		obj.push(content);
	}

	return writeFile(file, obj);
}
/**
 * Atualiza um item no array
 * @param {string} file 
 * @param {object} content 
 * @param {number} id 
 * ```ts
 * updateItem('user', { "name":"carlos pedro" }, 18);
 * ```
 */
function updateItem(file, content, id = null) {
	const obj = readFile(file);
	const index = obj.findIndex((item) => { return (id || content.id) == item.id });
	obj[index] = content;
	return writeFile(file, obj);
}
/**
 * Remove um item no array
 * @param {string} file 
 * @param {number} id 
 * ```ts
 * deleteItem( 'user', 15 );
 * ```
 */
function deleteItem(file, id) {
	let obj = readFile(file);
	const index = obj.findIndex(item => item.id == id);
	if (index == -1) return; // ID não encontrada
	obj.splice(index, 1);
	return writeFile(file, obj);
}



module.exports.mockRoutes = function (app) {

	///
	/// START
	///

	app.get('/read', function (_req, res) {
		res.json({ response: readFile('mocks/book') });
	});

	app.get('/write', function (_req, res) {
		res.json({ response: writeFile('mocks/book', { RANDOM: Math.random() }) });
	});

	app.get('/api/states', function (_req, res) {
		res.json({ response: readFile('mocks/states') });
	});

	app.route('/api/user')
		.get(function (_req, res) {
			res.json({ response: readFile('mocks/user') });
		})
		.post(function (_req, res) {
			res.json({ response: "inserido com sucesso" });
		})
		.delete(function (_req, res) {
			res.json({ response: "deletado com sucesso" });
		})
		.put(function (_req, res) {
			res.json({ response: "atualizado com sucesso" });
		});

	app.post('/api/', jsonParser, function (req, res) {
		if (typeof req.body['active'] !== 'boolean')
			res.json({ response: "[-ERROR-] Active: Precisa ser do tipo 'boolean' " })
		if (typeof req.body['name'] !== 'string')
			res.json({ response: "[-ERROR-] Name: Precisa ser do tipo 'String' " })
		if (typeof req.body['age'] !== 'number')
			res.json({ response: "[-ERROR-] Age: Precisa ser do tipo 'Number' " })
		else
			res.json({ response: "[-Sucess-] Requisição bem sucedida." })
	});

	app.route('/api/remedy/search')
		.get(function (_req, res) {
			if (_req.query)
				res.json({ response: filterItems('mocks/remedy', _req.query) });
		})

	// caminho default
	app.route('/api/:entity/search')
		.get(function (_req, res) {
			if (_req.query)
				res.json(filterItems(`mocks/${_req.params.entity}`, _req.query));
		});
	app.route('/api/*')
		.get(function (_req, res) {
			console.log('_req: ', _req.params[0]);
			res.json(getItems(`mocks/${_req.params[0]}`, _req.query || {}));
		})
		.post(function (_req, res) {
			res.json(createItem(`mocks/${_req.params[0]}`, _req.body));
		});
	app.route('/api/*/:id')
		.put(function (_req, res) {
			res.json(updateItem(`mocks/${_req.params[0]}`, _req.body, _req.params.id));
		})
		.delete(function (_req, res) {
			res.json(deleteItem(`mocks/${_req.params[0]}`, _req.params.id));
		});

	///
	/// END
	///
}
