// https://expressjs.com/pt-br/guide/routing.html
const express = require('express');
const cors = require('cors');
const app = express();
// const fs = require('fs');
// const data = JSON.parse(fs.readFileSync('user.json', 'utf8'));
// const states = JSON.parse(fs.readFileSync('states.json', 'utf8'));
// const path = require('path');
// const open = require('open'); // https://www.npmjs.com/package/opn
const bodyParser = require('body-parser');


const PORT = 4200;

//
// NODE EXPRESS 
// [EXPRESS](https://expressjs.com/pt-br/4x/api.html#router)
//

//
// CORS
//
const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200 // alguns navegadores legados (IE11, various SmartTVs)
}

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cors(corsOptions))

app.use(function (req, res, next) {
	let param = '[params] { '
	if (req.query)
		for (let key of Object.keys(req.query)) {
			param += `${key} : ${req.query[key]} , `
		}
	param += ' }';
	const date = new Date();
	const minutes = date.getMinutes().toString().length == 1 ? ('0' + date.getMinutes()) : date.getMinutes();
	console.log(`[Hour] ${date.getHours()}:${minutes} - [url] ${req.url} ${param}`);
	next();
});

// [[ROUTES]]
const routes = require('./routes.js');
routes.mockRoutes(app);


//
// VARIABLES
// As variaveis abaixo, são referentes ao navegador
//
// const directory = path.resolve(__dirname, '..', 'components');
// const file = directory + '/index.html';
// app.use('/', express.static(directory));



app.listen(PORT, function () {
	console.log(`[API][PORT: ${PORT}] - [SERVER START]`);
	// Opens the url in the default browser
	// open('http://localhost:3000/');
})