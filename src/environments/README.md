# Environments

Aqui deverão ir os arquivos referentes aos ambientes do sistema. O ambiente padrão para desenvolvimento é o **environments.ts**, enquanto o ambiente de produção é o **environments.prod.ts**. Qualquer informação como senha, hash ou chave que se deseje armazenar nesses arquivos deve ser inclusa como atributo da constante **environments** sendo exportada em cada arquivo:

```ts
export const environments = {
	...key: 'key_value',
}
```