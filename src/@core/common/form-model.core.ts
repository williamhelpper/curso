
export interface IFormModel {
	label: string;
	actions?: any;
	source: any[];
	setFormValue(objFormValue: any): void;
	showInputs(inputNames: string[]): void;
	hideInputs(inputNames: string[]): void;
}

/**
 * Classe chamada para criação de formulário, em conjunto com o
 * ```ts
 * // Exemplo de criação arquivo contendo a estrutura para criação do formulário *diseases-form.ts*
 * export const DISEASES_FORM = new FormModel();
 * DISEASES_FORM.label = 'Doenças';
 * DISEASES_FORM.source = [
 * 	{
 * 		type: 'input-text',
 * 		class: 'd-block mb-2',
 * 		name: 'cid',
 * 		props: {
 * 			label: 'CID',
 * 			value: 'A00.0 - Cólera',
 * 			required: true,
 * 		},
 * 	}
 * ]
 * // Exemplo de implentação
 * private selectItem(item): void {
 * 	const found = this.props.source.find(item => item.id == item);
 * 	DISEASES_FORM.setFormValue({
 * 		'cid': { value: found.CID },
 * 		'descricao': { value: found.descricao },
 * 		'capitulo': { value: found.capitulo },
 * 		'grupo': { value: found.grupo },
 * 		'doador': { checked: JSON.parse(found.doador) },
 * 	});
 * 	this.forceUpdate();
 * }
 * ```
 * ```html
 * <crud-form class="mt-2 d-block" props=${JSON.stringify(DISEASES_FORM)}></crud-form>
 * ```
 */
export class FormModel implements IFormModel {

	public label: string;
	public data: any;
	public actions: any = { save: undefined, delete: undefined };
	public source: any[];
	public editable?: boolean;
	public cardStyle?: string;
	public reactive?: boolean;
	public setData(data: any): void {
		this.data = data;
	}
	/**
	 * Atribui aos inputs os valores enviados no objeto. Considerando que o form tenha o seguinte source:
	 * ```ts
	 * [
	 * 	{
	 * 		type: 'input-text',
	 * 		props: {
	 * 			name: 'nome-pessoa',
	 * 		},
	 * 	},
	 * 	{
	 * 		type: 'checkbox-component',
	 * 		props: {
	 * 			name: 'ativo',
	 * 		},
	 * 	}
	 * ]
	 * ```
	 * O objeto a ser enviaddo no parâmetro deve ser:
	 * ```ts
	 * {
	 * 	'nome-pessoa': { value: 'João' },
	 * 	'ativo': { checked: true }
	 * }
	 * ```
	 * @param objFormValue Objeto com os valores a serem atribuídos
	 */
	public setFormValue(objFormValue: any): void {
		for (const key of Object.keys(objFormValue)) {
			const index = (this.source as any[]).findIndex(item => item.props.name == key);
			// console.log('search-key', key, 'objFormValue: ', objFormValue, 'this.source', this.source, 'this.source[index]: ', this.source[index]);
			if (this.source[index].type === 'select-search') { // Caso seja um SELECT
				(this.source[index].props.source as any[]).forEach(element => (element.value == objFormValue[key].value) ? (element.selected = true) : (element.selected = false));
			}
			else {
				this.source[index].props = { ...this.source[index].props, ...objFormValue[key] };
			}
		}
	}
	/**
	 * Exibe os inputs com os nomes enviados no parâmetro
	 * @param inputNames Nomes dos inputs a serem exibidos
	 */
	public showInputs(inputNames: string[]): void {
		// seta editable como undefined para não alterar o estado do form
		this.editable = undefined;
		inputNames.forEach(inputName => {
			const index = this.source.findIndex(item => item.props.name == inputName);
			if (index !== -1) {
				this.source[index].hide = false;
			}
		});
	}
	/**
	 * Esconde os inputs com os nomes enviados no parâmetro
	 * @param inputNames Nomes dos inputs a serem ocultados
	 */
	public hideInputs(inputNames: string[]): void {
		// seta editable como undefined para não alterar o estado do form
		this.editable = undefined;
		inputNames.forEach(inputName => {
			const index = this.source.findIndex(item => item.props.name == inputName);
			if (index !== -1) {
				this.source[index].hide = true;
			}
		});
	}
}
