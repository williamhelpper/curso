import { Router, Resolver } from '@vaadin/router';
/**
 * Utilizado para tipar os metodos de manipulação das rotas
 */
export interface IRouterCore {
	go(context, url);
	setContextAndRoutes(context, container, routes);
	getCurrentPath(context);
}
/**
 * Classe de controle do roteamento e navegação
 */
export class RouterCore implements IRouterCore {
	/**
	 * Returns the instance of the class
	 */
	static getInstance(): RouterCore {

		if (!RouterCore.instance) {
			RouterCore.instance = new RouterCore();
		}

		return RouterCore.instance;
	}
	private static instance: RouterCore;
	/**
	 * Cria um objeto, cuja chaves são os contextos, e seu valor são as instancias de Router
	 * Armazena uma instancia de Router, para um contexto, usado como chave
	 * { PAGES: Router(...), REGISTER: Router(...), ... }
	 */
	private static context: any = {
		ROOT: new Router(),
	};

	private constructor() { /* Services - cannot be instantiated by the 'constructor' */ }
	/**
	 * Renderiza uma pagina dentro de um contexto
	 * @param context string
	 * @param url string
	 */
	go(context, url: string) {
		if (this.getContextAndRoutes(context)) {
			(this.getContextAndRoutes(context) as Router).render(url);
		} else {
			console.log(`Contexto [${context}] de roteamento não encontrado`);
		}
	}
	/**
	 * Define o contexto, container e roteamento do sistema
	 * @param context string referente ao contexto que armazena os dados de roteamento
	 * @param container elemento que irá renderizar o conteudo do roteamento
	 * @param routes array contendo as rotas do sistema
	 */
	setContextAndRoutes(context: string, container, routes) {
		RouterCore.context[context] = this.setContainerAndRoutes(container, routes);
		console.log('RouterCore.context: ', RouterCore.context);
	}
	addRoutesInContext(context: string, routes: Router.Route[]) {
		// console.log('routes: ', routes);
		// console.log('context: ', context);
		// // let route = (RouterCore.context[context] as Router);

		// if (!RouterCore.context[context]) {

		// }

		// console.log('route: ', route);
		// // const mac =
		// route.addRoutes([...routes]);
		// console.log('mac: ', mac);

		// const resolver = new Resolver([...routes]);
		// console.log('resolver: ', resolver);

		// const customRoutes: Router = new Router();
		// customRoutes.setRoutes([...routes]);

		// const brabo = (RouterCore.context[context] as Router).addRoutes(resolver.getRoutes());
		// console.log('brabo: ', brabo);
		// console.log('l: ', l);
		// (RouterCore.context[context] as Router).setRoutes(routes);
		// (RouterCore.context[context] as Router).
		// (RouterCore.context[context] as Router).ready.then( r => console.log('r ',r))
		// console.log('this.context: ', RouterCore.context);
		// console.log('this.context: ', RouterCore.context['ROOT']);

		// const container = route.getOutlet();
		// this.setContainerAndRoutes(container, [...routes]);

		// // route.getOutlet()
		// // console.log('getOutlet ', route.getOutlet());
		// console.log('getRoutes ', route.getRoutes());
		// route.
		// console.log('getRoutes ', route.baseUrl());
	}
	/**
	 * Retorna o path atual de um contexto
	 * @param context string referente ao contexto
	 */
	public getCurrentPath(context: string) {
		return (this.getContextAndRoutes(context) as Router).location.route.path;
	}
	/**
	 * Recupera a classe de roteamento com base no contexto passado
	 * @param context string
	 */
	private getContextAndRoutes(context) {
		return RouterCore.context[context] || null;
	}
	/**
	 * Retorna uma instancia de Router
	 * @param container elemento que irá conter as rotas
	 * @param routes array contendo as rotas do sistema
	 */
	private setContainerAndRoutes(container, routes) {
		const route = new Router(container);
		route.setRoutes([...routes]);
		return route;
	}

}
