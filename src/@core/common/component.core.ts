import { LitElement, css, property, html } from 'lit-element';
import { BOOTSTRAP__CSS } from '../../assets/css/global-css';
import { LanguageService } from '../services/language.service';

export class BaseComponent extends LitElement {

	@property({ type: String }) public set props(props) {
		if (props !== 'undefined') {
			const properties = typeof props === 'string' ? JSON.parse(String(props)) : props;

			this.directives.forEach((tag) => {
				if (properties[tag] ||
					properties[tag] === false ||
					properties[tag] === '' ||
					properties[tag] === 0) {
					this[tag] = properties[tag];
				}
			});
			this.forceUpdate();
		}
	}

	// Transforma o componente em light dom
	// createRenderRoot() { return this; }

	/**
	 * Define estilo global para aplicação,
	 * Caso existam outros estilos globais podem ser adicionais
	 * [EXEMPLO] static get styles() {return [super.styles,css`:host { background-color:green!important; }`];}
	 **/
	static get styles(): any[] {
		// @ts-ignore
		return [css`${BOOTSTRAP__CSS}`];
	}
	/**
	 * 	Gera um elemento html conforme o componente passado como parametro
	 * @method static
	 * @param component { component: string; props: any; }
	 * @return elemenent html [component]
	 */
	static createElementWithSelectorAndProps(component: { component: string; props: any; }) {
		const factoryComponent = document.createElement(component.component) as HTMLElement;
		component.props && factoryComponent.setAttribute('props', JSON.stringify(component.props));
		return factoryComponent;
		// Cria um componente com base em seu nome e passa a ele props
		// let factoryComponent = document.createElement(component.component) as HTMLElement;
		// component.props && factoryComponent.setAttribute('props', JSON.stringify(component.props));

		// (component.bind) && // se existir o atributo bind: true, informando que esse componente está usando o recurso de two way data bind, a função abaixo iria escutar o evento
		// factoryComponent.addEventListener('input-bind', function (bind: any) { console.log('[input-bind] ', bind.detail) })
		// this.events.push(factoryComponent)
	}

	directives: string[] = [];

	/**
	 * @input-bind
	 * Essa função é utilizada para fazer o two way data binding
	 * retornar os dados de um input dentro do componente filho para o componente pai
	 * [EXEMPLO]: FAZENDO BIND DA PROPRIEDADE [myInput] do componente pai
	 * <input-text-component @input-bind=${bind => this.login.user = bind.detail}}></input-text-component>
	 * ao sofrer alteração o componente filho atribui valor a propriedade [myInput]
	 * [EXEMPLO]
	 * captura alteração @change="${this.onChange}"
	 * 		<input @change="${this.onChange}" name=${this.name} id=${this.id} >
	 * faz o bind e atribui o valor
	 * 		private onChange() { this.bindModel(this.inputEl.value) }
	 * recupera o elemento html e retorna ele
	 * 		private get inputEl(): HTMLInputElement {return this.shadowRoot!.getElementById(this.id)! as HTMLInputElement;}
	 *
	 * @param value { any }
	 */
	bindModel(value: any) {
		this.dispatchEvent(new CustomEvent('input-bind', { detail: value }));
	}

	customBindModel(eventName: string, value: any) {
		this.dispatchEvent(new CustomEvent(eventName, { detail: value }));
	}

	/**
	 * Lifecycles
	 * 1 onInit
	 * 2 onBeforeUpdate
	 * 3 onAfterViewInit
	 * 4 onChanges
	 *
	 * after update
	 * 5 onBeforeUpdate
	 * 6 onChanges
	 *
	 * last
	 * 7 onDestroy
	 */

	/**
	 * É chamado quando o componente é inserido no Dom
	 */
	onInit() {
		// console.log('onInit: ');
	}
	/**
	 * É chamado quando o componente é destruído
	 */
	onDestroy() {
		// console.log('onDestroy: ');
	}
	/**
	 * É chamado antes do onChanges() para controlar as atualizações do componente, fazer validações etc. Retorna obrigatoriamente um boolean
	 * Caso ele retorne false, a função onChanges() não será chamada
	 */
	onBeforeUpdate(): boolean {
		// console.log('onBeforeUpdate: ');
		return true;
	}
	/**
	 * É chamado quando as propriedades sofrem alteração durante o ciclo de vida do componente
	 * @param changedProps listener de todas as propriedades do componente
	 */
	onChanges(changedProps: any) {
		// console.log('onChanges: ', changedProps);
	}
	/**
	 * É chamado após o componente renderizar pela primeira vez, e antes de emitir alguma alteração, pode ser usado para fazer buscas no dom, e produzir alterações nos elementos
	 */
	onAfterViewInit() {
		// console.log('onAfterViewInit: ');
	}
	/**
	 * Força atualização do dom, usado quando existe alteração em variavel e ela não reflete no dom
	 */
	forceUpdate() {
		this.requestUpdate();
	}

	/**
	 * Os metodos abaixo foram abstraídos para simplificar suas chamadas
	 */

	/**
	* The connectedCallback is called when the element is inserted to the DOM.
	*/
	connectedCallback() {
		super.connectedCallback();
		this.onInit();
	}
	/**
	 *  Implement to control if updating and rendering should occur when property values change or requestUpdate() is called.
	 */
	shouldUpdate() {
		return this.onBeforeUpdate();
	}
	/**
	* Called after the element's DOM has been updated the first time, immediately before updated()
	* is called. This method can be useful for querying dom. Setting properties inside
	* this method will trigger the element to update.
	*/
	firstUpdated() {
		this.onAfterViewInit();
	}
	/**
	* Implement to perform post-updating tasks via DOM APIs, for example, focusing an element.
	* Setting properties inside this method will *not* trigger another update.
	*/
	updated(changedProps: any) {
		super.updated(changedProps);
		this.onChanges(changedProps);
	}
	/**
	* Invoked each time the custom element is disconnected from the document's DOM.
	* Useful for running clean up code.
	*/
	disconnectedCallback() {
		this.onDestroy();
	}

}

/**
 * InputBaseComponent
 */
export interface IInputBaseComponent {
	name: string;
	label: string;
	submit: string;
	__submit: boolean;
	input: HTMLInputElement;
	validation();
}

export class InputBaseComponent extends BaseComponent implements IInputBaseComponent {
	/**
	 * Seta a propriedade value, na tag, caso ela seja passada irá popular o elemento assim que o mesmo for renderizado
	 */
	@property() public set value(value) {
		if (this.value !== value) {
			this.__value = value;
			if (this.input) {
				this.input.value = this.value;
				this.validation();
			}
		}
	}
	public get value() { return this.__value; }
	/**
	 * Recupera o elemento input da pagina, através do id dinâmico gerado
	 */
	public get input(): HTMLInputElement { return this.shadowRoot!.getElementById(`input-${this.name}`)! as HTMLInputElement; }
	/**
	 * Diretiva de submit, é usado para controlar o input, informando os campos que o formulário já foi submetido de forma a eles fazerem as validações e retornarem os valores
	 */
	@property() get submit() { return this.__submit; }
	set submit(submit) {
		this.__submit = JSON.parse(submit);
		this.forceUpdate();
		this.validation();
	}

	static get styles() {
		// @ts-ignore
		return [super.styles, css`
		input:focus {
			outline-offset: 0;
			outline: 0;
		}
		input{
			height: 28px;
			border-radius: var(--radius-sm);
			border: 1px solid var(--gray-dark);
			padding: 0 10px;
    		color: var(--dark-light);
		}
		input:read-only {
			background-color: var(--white);
		}
		label{
			display: flex;
			flex-direction: column;
			color: var(--dark-dark);
			margin-bottom: 0px;
		}
		div.label { font-size: var(--font-sm); }
	`];
	}
	/**
	 * Diretivas para props
	 */
	directives: string[] = ['min', 'max', 'name', 'label', 'required', 'value', 'readonly'];
	/**
	 * Faz a validação de numero minimo de caracteres; [EXEMPLO] min="10" [OPCIONAL]
	 */
	@property({ type: String }) min;
	/**
	 * Faz a validação de numero maximo de caracteres; [EXEMPLO] min="10" [OPCIONAL]
	 */
	@property({ type: String }) max;
	/**
	 * Seta a propriedade name dos inputs
	 */
	@property({ type: String }) name = 'name_default';
	/**
	 * Seta o label dos inputs
	 */
	@property({ type: String }) label = '';
	/**
	 * Faz a validação do input tornando o mesmo required [OPCIONAL]
	 */
	@property({ type: Boolean }) required;
	/**
	 * Torna o input não-editável [OPCIONAL]
	 */
	@property({ type: Boolean }) readonly;
	public __submit;
	/**
	 * Exibe a mensagem de erro na tela, é chamado pela função [isError(errorMessage)]
	 */
	protected __errorMessage = '';
	/**
	 * Se existir erro irá retornar true no binding
	 * @return boolean
	 */
	protected __inputError: boolean = false;
	/**
	 * Armazena valor do input sem a mascara
	 */
	protected valueNotMask: string = null;
	private __value;

	render() {
		return html`
			<label for="input-${this.name}">
				${/*__LABEL__*/this.label ? html`<div class="label">${LanguageService.translate(this.label)}</div>` : ''}
				${this.getInput()}
				${/*__ERROR-MSG__*/this.__errorMessage ? html`<small class="msg--error danger">${LanguageService.translate(this.__errorMessage)}</small>` : ''}
			</label>
		`;
	}

	onAfterViewInit() {
		/**
		 * Popula o value do input apos inicializar
		 */
		this.entryValue();
	}
	/**
	 * Popula o value do input apos inicializar, caso a propriedade tenha sido passado por parametro
	 */
	entryValue() {
		if (this.value) {
			this.input.value = this.value;
			this.maxValidate();
		}
	}
	/**
	 * Seta o retorno de error como true e escreve a mensagem de erro
	 * @param errorMessage { string }
	 */
	isError(errorMessage) {
		this.__errorMessage = errorMessage;
		this.__inputError = true;
	}
	/**
	 * Limpa a mensagem de erro, e seta o retorno de error como false
	 */
	clearError() {
		this.__inputError = false;
		this.__errorMessage = '';
	}

	public validation() {
		if (!this.input) { return; } // só efetua validação se encontrar o input
		this.clearError();
		if (this.submit) {
			this.isRequired();
			if (this.input.value.length > 0) {
				this.minValidate();
				this.customValidate();
			}
		}
		this.maxValidate();
		this.forceUpdate();

		// Apos validar os dados faz o data-binding
		// Se um valor sem a mascara for passado em [this.valueNotMask] irá retornar ele e não o value do input
		super.bindModel({ value: (this.valueNotMask) ? this.valueNotMask : this.input.value, error: this.__inputError });
	}
	/**
	 * Retorna o html do input, de forma a poder alterar somente ele no template, não mexendo no label nem nas mensagens de erro
	 */
	protected getInput() {
		return html`<input @keyup="${this.validation}" id="input-${this.name}" name=${this.name} ?readonly=${this.readonly} >`;
	}
	/**
	* Valida se o campo é requerido
	*/
	protected isRequired() {
		if (this.required && this.input.value.length == 0) {
			this.isError(`${LanguageService.translate('Campo é requerido')}`);
		}
	}
	/**
	* Valida o campo minimo
	*/
	protected minValidate() {
		if (this.min && this.input.value.length < this.min) {
			this.isError(`${LanguageService.translate('Mínimo de')} ${this.min} ${LanguageService.translate('caracteres')}`);
		}
	}
	/**
	* Valida o tamanho maximo
	*/
	protected maxValidate() {
		if (this.max && this.input.value.length > this.max) {
			this.input.value = this.input.value.substring(0, this.max);
		}
	}
	/**
	* Faz uma validação customizada
	* É chamado no momento em que chama a função validation, quando o @keyup, e quando é atribuido o submit
	*/
	protected customValidate() { }
}

export class Location {
	// static Routes: any;
	// static setRoutes: any;
	/**
	 * Navega a uma página, opcionalmente definindo parâmetros de pesquisa
	 *
	 * ```ts
	 * //EXEMPLO
	 * Location.goPage('/pages/test', { name: 'João', age: 20 });
	 * ```
	 * @param url string - caminho da página
	 * @param params any - objeto de querystrings (opcional)
	 */
	public static goPage(url, params?) {
		// window.location.href = url;
		if (params) {
			url = Location.setParams(url, params);
		}
		const anchor = document.createElement('a');
		anchor.setAttribute('href', url);
		document.querySelector('body').appendChild(anchor);
		anchor.click();
		anchor.remove();
	}
	/**
	 * Retorna os parâmetros de consulta da página atual
	 */
	public static getParams(): any {
		const search = Location.getSearch();
		// "?name=jatoba"
		const obj: any = {};
		if (search) {
			search.replace('?', '').split('&').map(item => {
				const attribute = item.split('=');
				obj[attribute[0]] = attribute[1];
			});
		}
		return obj;
	}
	/**
	 * getHref
	 * [EXEMPLO] http://localhost:3000/pages/generate
	 * return { string }
	 */
	public static getHref() { return window.location.href; }
	/**
	 * getHost
	 * [EXEMPLO] localhost:3000
	 * return { string }
	 */
	public static getHost() { return window.location.host; }
	/**
	 * getHostName
	 * [EXEMPLO] localhost
	 * return { string }
	 */
	public static getHostName() { return window.location.hostname; }
	/**
	 * getPathName
	 * [EXEMPLO] /pages/generate
	 * return { string }
	 */
	public static getPathName() { return window.location.pathname || ''; }
	/**
	 * getHash
	 * [EXEMPLO] /
	 * return { string }
	 */
	public static getHash() { return window.location.hash || ''; }
	/**
	* getSearch
	* [EXEMPLO] search: "?id=464564"
	* return { string }
	*/
	public static getSearch() { return window.location.search || ''; }
	/**
	 * getLocation
	 * return Location
	 */
	public static getLocation() { return window.location || ''; }
	/**
	 * Retorna uma URL com os parâmetros como query string
	 * @param url string - path
	 * @param params any - parâmetros para ser inseridos na url
	 */
	private static setParams(url, params): string {
		Object.keys(params).map((key, index) => {
			url += `${index > 0 ? '&' : '?'}${key}=${params[key]}`;
		});
		return url;
	}
}

/**
 * Decorators
 */
// function render<MethodyDecorator>(target: Object,
// 	                                 propertyName: string,
// 	                                 propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
// 	console.log("target: ", target);
// 	console.log("propertyName: ", propertyName);
// 	console.log("propertyDesciptor: ", propertyDesciptor);
// 	// const method = propertyDesciptor.value;
// 	// propertyDesciptor.value = function (...args: any[]) {

// 	// 	// convert list of greet arguments to string
// 	// 	const params = args.map(a => JSON.stringify(a)).join();

// 	// 	// invoke greet() and get its return value
// 	// 	const result = method.apply(this, args);

// 	// 	// convert result to string
// 	// 	const r = JSON.stringify(result);

// 	// 	// display in console the function call details
// 	// 	console.log(`Call: ${propertyName}(${params}) => ${r}`);

// 	// 	// return the result of invoking the method
// 	// 	return result;
// 	// }
// 	return propertyDesciptor;
// }

// function Component(constructor: Function) {
// 	// Object.seal(constructor);
// 	console.log('constructor: ', constructor);
// 	// Object.seal(constructor.prototype);
// 	console.log('constructor.prototype: ', constructor.prototype);
// 	let x = constructor.prototype.getToken()
// 	console.log('x: ', x);
// }

// component{
// 	service:Service
// 	constructor( service: Service){
// 		this.service = new Service();
// 	}
// }

// export const Component = <TBase extends { new(...args: any[]): {} }>(Base: TBase): TBase => {
// 	return class extends Base {
// 		constructor(...args: any[]) {
// 			super(...args);
// 		}
// 	}
// }

// type Constructor<T = {}> = new (...args: any[]) => T;
// export function Component<TBase extends Constructor>(Base: TBase) {
// 	console.log('Base: ', Base);
// 	// console.log('Base typeof: ', typeof Base);
// 	// console.log('Base: ', (Base.prototype as HTMLElement).attributes );
// 	// console.log('Base: ', Base.prototype.constructor);
// 	return class extends Base {
// 		constructor(...args: any[]) {
// 			console.log('...args: ', ...args);
// 			super(...args);
// 		}
// 	};
// }

// const requiredMetadataKey = Symbol("required");
// function required(target: Object, propertyKey: string | symbol, parameterIndex: number) {
//     let existingRequiredParameters: number[] = Reflect.getOwnMetadata(requiredMetadataKey, target, propertyKey) || [];
//     existingRequiredParameters.push(parameterIndex);
//     Reflect.defineMetadata(requiredMetadataKey, existingRequiredParameters, target, propertyKey);
// }

// export function annotation(args) {
// 	let c = args
// 	// c[0]
// 	console.log('c[0]: ', c[0]);
// 	console.log('ct ', c.token);
// 	// args.map( item => console.log(`ars + ${item} +`) )
// }

// export function ParamTypes(...types) {
// 	// as propertyKey is effectively optional, its easier to use here
// 	return (target, propertyKey) => { Reflect.defineMetadata("design:paramtypes", types, target, propertyKey); }
// }

// function getParams() {
// 	const newInstance = Object.create(window['className'].prototype);
// 	// newInstance.constructor.apply(newInstance, instanceparameters);
// 	return newInstance;
// }

// export function Component(target) {
// 	console.log('target: ', target);
// 	const types = Reflect.getMetadata('design:paramtypes', target);
// 	console.log('types: ', types);
//     // return a modified constructor
// }

// export const Component = <TBase extends { new(...args: any[]): {} }>(Base: TBase): TBase => {
// 	console.log('TBase: ', Base);
// 	console.log('TBase: ', Base.prototype.constructor );
// 	console.log('TBase: ', Base.prototype);
// 	// getParams(Base)

// 	return class extends Base {

// 		constructor(...args: any[]) {
// 			console.log('Injectable ...args ', ...args);
// 			super(...args);
// 		}
// 	}
// }

// interface ClassType<InstanceType extends {} = {}> extends Function {
// 	new(...args: any[]): InstanceType
// 	prototype: InstanceType
// }

// type InstanceType<T extends new (...args: any[]) => any> = T extends new (...args: any[]) => infer R ? R : any;

// const AddPropertiesDecorator = <BaseClass extends ClassType<{}>, BaseInstance extends InstanceType<BaseClass>>(base: BaseClass) =>
// 	class extends base {
// 		values!: BaseInstance[]
// 		add(value: BaseInstance) {
// 			this.values.push(value)
// 		}
// 	}

// export const Component = <TBase extends { ClassType }>(Base: ClassType): TBase => {
// 	return class extends AddPropertiesDecorator(aT:T) {
// 		constructor(...args: any[]) {
// 			super(...args);
// 		}
// 	}
// }

// class ItemList extends AddPropertiesDecorator(Item) {
// 	render() {
// 		this.values.forEach((item) => item.render())
// 	}
// }
// const list = new ItemList()
// console.log(list.values)
// list.render()

// const ANNOTATIONS = '__annotations__';

// export function Injectable() {
// 	function DecoratorFactory(cls: any, objOrType?: any) {
// 		const annotationInstance = (<any>DecoratorFactory)objOrType
// 		const annotations = cls.hasOwnProperty(ANNOTATIONS) ? (cls as any)[ANNOTATIONS] :
// 			Object.defineProperty(cls, ANNOTATIONS, { value: [] })[ANNOTATIONS];
// 		annotations.push(annotationInstance);
// 		return cls;
// 	}
// 	return DecoratorFactory
// }

// class ReflectiveInjector {
//     private static records: { token:any, deps:any }[] = []
//     static resolveAndCreate(tokens: Array<any>) {
//         tokens.forEach((token:any)=> {
//             ReflectiveInjector.records.push({
//                 token,
//                 deps: Reflect.getOwnMetadata('design:paramtypes', token)
//             })
//         })
//         return this
//     }
//     static get(_token: any) {
//         // get the `token` from the record set
//         const [record] = ReflectiveInjector.records.filter((record)=>{
//             return record.token == _token
//         })
//         let {token, deps} = record
//         // resolve dependencies into instances
//         deps = deps.map((dep: any)=>{ return new dep() })
//         // create the instance of the token with the resolved dependencies
//         return new token(...deps)
//     }
// }
