import { ApiCore, ENDPOINTS } from './api.module';
/**
 * Classe base para Api's
 */
export class BaseApi {

	public static entity: string;

	/**
	 * Retorna uma lista de itens da api
	 * @param params object
	 */
	public static list(params = null) {
		return ApiCore.call(ENDPOINTS[this.entity].GETALL, params);
	}
	/**
	 * Recupera detalhes de um item
	 * @param idOrConcatPath number or complete path ex:. path/1 || path/1/256
	 */
	public static get(idOrConcatPath) {
		// return ApiCore.call(ENDPOINTS[this.entity].GET + '/' + idOrConcatPath);
		return ApiCore.call(ENDPOINTS[this.entity].GET + '/search' , idOrConcatPath);
	}
	/**
	 * Cria um item na api
	 * @param body object
	 */
	public static create(body) {
		return ApiCore.call(ENDPOINTS[this.entity].POST, JSON.stringify(body), 'POST');
	}
	/**
	 * Atualiza item
	 * @param body object
	 */
	public static update(body) {
		return ApiCore.call(ENDPOINTS[this.entity].PUT, JSON.stringify(body), 'PUT');
	}
	/**
	 * Deleta um item
	 * @param idOrConcatPath number or complete path ex:. path/1 || path/1/256
	 */
	public static delete(idOrConcatPath) {
		return ApiCore.call(ENDPOINTS[this.entity].DELETE, idOrConcatPath, 'DELETE');
	}
	/**
	 * This function encodes special characters, except: , / ? : @ & = + $ #
	 * @param data
	 */
	public static encodeURI(data) {
		return encodeURI(data);
	}
	/**
	 * This function decodes special characters
	 * @param data
	 */
	public static decodeURI(data) {
		return decodeURI(data);
	}

}
export class BulkBaseApi extends BaseApi {
	/**
	 * Cria múltiplos itens na api
	 * @param body object
	 */
	 public static createBulk(body) {
		const endpoint = ENDPOINTS[this.entity].POST + (ENDPOINTS[this.entity].POST.endsWith('/') ? '' : '/');
		return ApiCore.call(endpoint + 'bulk', JSON.stringify(body), 'POST');
	}
	/**
	 * Deleta múltiplos itens na api
	 * @param body object
	 */
	public static deleteBulk(body) {
		const endpoint = ENDPOINTS[this.entity].DELETE + (ENDPOINTS[this.entity].DELETE.endsWith('/') ? '' : '/');
		return ApiCore.call(endpoint  + 'bulk', JSON.stringify(body), 'DELETE');
	}
}
/**
 * Classe usada para trabalhar com os dados mocados na api de testes
 */
export class MockApi {

	public static entity: string;

	public static list(params = null) {
		// let args = { page: (params && params.page) ? params.page : 1, limit: (params && params.limit) ? params.limit : 10, ...params }
		const args = { page: (params && params.page) ? params.page : 1, ...params };
		return ApiCore.call(ENDPOINTS[this.entity].GETALL, params);
	}

	public static get(id) {
		return ApiCore.call(ENDPOINTS[this.entity].GET + id);
	}

	public static createOrUpdate(body) {
		if (body.id) {
			return ApiCore.call(ENDPOINTS[this.entity].PUT + body.id, JSON.stringify(body), 'PUT');
		} else {
			return ApiCore.call(ENDPOINTS[this.entity].POST, JSON.stringify(body), 'POST');
		}
	}

	// public static updateEntity(id) {
	// 	return ApiCore.call(ENDPOINTS[this.entity].PUT + id);
	// }

	public static delete(id) {
		return ApiCore.call(ENDPOINTS[this.entity].DELETE, id, 'DELETE');
	}

	/**
	 * This function encodes special characters, except: , / ? : @ & = + $ #
	 * @param data
	 */
	public static encodeURI(data) {
		return encodeURI(data);
	}

	/**
	 * This function decodes special characters
	 * @param data
	 */
	public static decodeURI(data) {
		return decodeURI(data);
	}

}
