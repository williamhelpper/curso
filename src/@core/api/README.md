# Api

Aqui devem ir os arquivos referentes a APIs do sistema Populis. Todo provider criado deve ser incluso no arquivo **api.module.ts**:

```ts
import { ApiCore } from './api.core';
import { ENDPOINTS } from './endpoints.core';

export {
	ApiCore,
	ENDPOINTS,
}

export class ApiModule { }
```

# Implementação

```ts
import * as api from '../../../@core/api/api.module';
api.ApiCore.call(api.ENDPOINTS.cep.GET, {})
	.then(data => console.log(data))
	.catch(error => console.log(error))
```

## Core API

No arquivo **api.core.ts** devem ser desenvolvidas todas as funções essenciais para o funcionamento das requisições.

## Endpoint

No arquivo **endpoints.core.ts** deve conter as URLs para todos os endpoints necessários ao funcionamento das requisições.