import { ApiCore, ENDPOINTS } from './api.module';
import { base64enconde } from '../services/utils.service';

export class AuthApi {

	public static login(data: { username: string, password: string }): Promise<any> {
		return ApiCore.call(ENDPOINTS.auth.login, { Authorization: 'Basic ' + base64enconde(data.username + ':' + data.password) });
	}

	public static logout(): Promise<any> {
		return ApiCore.call(ENDPOINTS.auth.logout, null, 'DELETE');
	}

	public static getPage(): Promise<any> {
		return ApiCore.call('https://cq04.populisservicos.com.br/populis/api/v2/sso/ppra/ssogru');
	}

}
