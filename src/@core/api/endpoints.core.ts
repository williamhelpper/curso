import { environments } from '../../environments/environments';

const BASE_URL = environments.api;
const marcos = environments.marcos;
export const ENDPOINTS = {
	auth: {
		// login: 'https://cq04.populisservicos.com.br/populis/api/auth',
		login: `${environments.marcosAuth}auth`,
		logout: `${environments.apiAuth}auth`,
	},
	products: {
		GET: 'http://localhost:4200/api/products', /* /:id required */
		GETALL: 'http://localhost:4200/api/products',
		POST: 'http://localhost:4200/api/products',
		PUT: 'http://localhost:4200/api/products', /* /:id required */
		DELETE: 'http://localhost:4200/api/products', /* /:id required */
	},
};
