// import { expect } from '@open-wc/testing';
// import { UsersApi } from '../../../@core/api/users.api';

// const users: UsersApi = new UsersApi();
// let id: number;
// const user = {
//     "createdAt": `${new Date().toUTCString()}`,
//     "name": `Florindo da data ${new Date().getTime()}`,
//     "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/msveet/128.jpg",
//     "email": `florindo1@mail.com`
// };

// describe('describe tests in UsersAPI', function() {

//     it('[UsersAPI] should list users', async function() {
//         const request = await users.listUsers()
//             .then(dataUsers => { return dataUsers; })
//             .catch(error => { console.log('[UsersAPI] Request error: ', error); return null; });

//         expect(request).to.not.be.null;

//         expect(request.length).to.be.above(0);
//         id = request.length + 1;
//         user['id'] = String(id);
//     });

//     it('[UsersAPI] should create user', async function() {
//         if (!id) { return this.skip(); }

//         const request = await users.createUser(user)
//             .then(dataUsers => { return dataUsers; })
//             .catch(error => { console.log('[UsersAPI] Request error: ', error); return null; });

//         expect(request).to.not.be.null;
//         expect(request).to.deep.equal(user);
//     });

//     it('[UsersAPI] should get user', async function() {
//         if (!id) { return this.skip(); }

//         const request = await users.getUser(id)
//             .then(dataUser => { return dataUser; })
//             .catch(error => { console.log('[UsersAPI] Request error: ', error); return null; });

//         expect(request).to.not.be.null;
//         expect(request).to.deep.equal(user);
//     });

//     it('[UsersAPI] should update user', async function() {
//         if (!id) { return this.skip(); }
//         const pastName = user.name;
//         const request = await users.updateUser(Object.assign(user, { name: user.name + ' UPDATED' }))
//             .then(dataUser => { return dataUser; })
//             .catch(error => { console.log('[UsersAPI] Request error: ', error); return null; });

//         expect(request).to.not.be.null;
//         expect(request.name).to.be.not.equal(pastName);
//     });

//     it('[UsersAPI] should delete user', async function() {
//         if (!id) { return this.skip(); }
//         const request = await users.deleteUser(id)
//             .then(dataUser => { return dataUser; })
//             .catch(error => { console.log('[UsersAPI] Request error: ', error); return null; });

//         expect(request).to.not.be.null;
//     });

// });
