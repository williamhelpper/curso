import { ApiCore } from './api.core';
import { ENDPOINTS } from './endpoints.core';

export {
	ApiCore,
	ENDPOINTS,
};

export class ApiModule { }
