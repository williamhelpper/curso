import { ajax } from 'rxjs/ajax';
import { map, catchError, take } from 'rxjs/operators';
import { of } from 'rxjs';
import { environments } from '../../environments/environments';

const BASE_URL = environments.api;

export class ApiCore {

	public static call(url: string, parameters: any = null, method: 'POST' | 'DELETE' | 'PUT' | 'GET' = 'GET', optionalParams: any = {}) {

		let headers: any = { headers: new Headers() };
		headers.headers.append('Content-Type', 'application/json');
		headers.headers.append('Accept', 'application/json');

		// Verifica se está batendo na url da populis para enviar as Crenciais
		// Caso seja diferente de localhost, ou seja em desenvolvimento, sempre irá pedir as credenciais
		if (!url.includes('localhost')) {
			optionalParams.withCredentials = true;
			optionalParams.credentials = 'include';
			optionalParams.mode = 'cors';
		}

		if (method == 'GET') {
			(url.endsWith('/')) ? url.substring(url.length - 1, 1) : url;
			if (parameters && parameters.Authorization) {
				headers.headers.append('Authorization', parameters.Authorization);
			} else {
				url += this.prepareParams(parameters);
			}
		}

		if (method == 'POST' || method == 'PUT') {
			if (parameters) { headers.body = parameters; }
		}

		if (method == 'DELETE') {
			// Verifica se parameters é um objeto ou um array (se sim, 'String(parameters)' retornará '[object Object]')
			if (parameters && String(parameters).substring(0, 1) !== '[') {
				(url.endsWith('/')) ? url += parameters : url += '/' + parameters;
				headers.body = JSON.stringify({});
			} else {
				if (parameters) { headers.body = parameters; }
			}
		}

		headers = {
			url,
			method,
			...optionalParams,
			...headers,
		};

		return fetch(url, { ...headers })
			.then(response => {
				// console.log('FETCH ', url, 'response ', response);
				return response.json();
			})
			.then(result => {
				// console.log('FETCH', url, 'data', result);
				return result;
			})
			.catch(error => {
				console.log('error', error);
				return error;
			});

	}
	/**
	 * call
	 * Metodo usado para fazer requisições
	 * Quando for passar parametros via GET, passe como objeto em data
	 *
	 * @see https://rxjs-dev.firebaseapp.com/api/ajax/ajax
	 *
	 * @param url {string}
	 * @param parameters {objeto} 'null' = DEFAULT
	 * @param method {'POST' | 'DELETE' | 'PUT' | 'GET'} 'GET' = DEFAULT
	 * @param optionalParams {objeto}
	 *
	 * @return promise
	 */
	// public static call(url: string, parameters: any = null, method: 'POST' | 'DELETE' | 'PUT' | 'GET' = 'GET', optionalParams: any = {}) {
	// 	let headers = { headers: {} }
	// 	headers.headers['Content-Type'] = 'application/json';
	// 	headers.headers['Accept'] = 'application/json';

	// 	if (method == 'GET') {
	// 		(url.endsWith('/')) ? url.substring(url.length - 1, 1) : url;
	// 		if (parameters && parameters['Authorization']) {
	// 			headers.headers['Authorization'] = parameters['Authorization'];
	// 		} else {
	// 			// Verifica se está batendo na url da populis para enviar as Crenciais
	// 			// Em produção e ao mudar o endpoint remova esse trecho
	// 			if (url.includes(BASE_URL)) {
	// 				optionalParams['withCredentials'] = true;
	// 				optionalParams['credentials'] = 'include';
	// 				optionalParams['crossDomain'] = true;
	// 			}
	// 			url += this.prepareParams(parameters);
	// 		}
	// 	}

	// 	if (method == 'POST' || method == 'PUT')
	// 		if (parameters) headers['body'] = parameters;

	// if (method == 'DELETE') {
	// 	(url.endsWith('/')) ? url += parameters : url += '/' + parameters;
	// 	headers['body'] = {}
	// }

	// 	headers = {
	// 		url,
	// 		method,
	// 		...optionalParams,
	// 		...headers,
	// 		createXHR: function () {
	// 			return new XMLHttpRequest();
	// 		}
	// 	}
	// 	console.log('@@API@@ headers: ', headers);

	// 	return ajax({ ...headers }).pipe(
	// 		map(map => map.response),
	// 		take(1),
	// 		catchError(error => of(error)))
	// 		.toPromise();

	// }

	/**
     * Converte objeto para parametros de url
     */
	private static prepareParams(params) {
		let prepareParams = '';
		let conector = '?';
		for (const key in params) {
			prepareParams += conector + key + '=' + params[key];
			conector = '&';
		}
		return prepareParams;
	}

}
