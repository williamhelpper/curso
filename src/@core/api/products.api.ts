import { ApiCore, ENDPOINTS } from './api.module';
import { BaseApi } from './api.base';

export class ProductsApi extends BaseApi{
	public static entity: string = 'products';
}
