# @Core

Desenvolvimento de recursos essenciais ao funcionamento do sistema.

## Testes

Testes referentes à APIs ou services devem ser criados dentro de uma sub-pasta única chamada `tests` dentro da pasta `api` ou `services`:

```
services/
├── storage/
│   └── storage.service.ts
├── tests/
│   ├── cookie.service.spec.ts
│   └── storage.service.spec.ts
├── base.service.ts
├── cookie.service.ts
├── crypto.service.ts
└── utils.service.ts
```