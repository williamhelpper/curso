interface ISingleton {
	getInstance();
}

/* class decorator */
function staticImplements<T>() {
	return <U extends T>(constructor: U) => { constructor; };
}

@staticImplements<ISingleton>()
export class BaseService {

	/**
	 * Returns the instance of the class
	 */
	public static getInstance(): BaseService {

		if (!BaseService.instance) {
			BaseService.instance = new BaseService();
		}

		return BaseService.instance;
	}
	private static instance: BaseService;

	constructor() { /* Services - cannot be instantiated by the 'constructor' */ }
}
