import { StorageService } from './storage.service';

export class SsoStorageService {

	/**
	 * Armazena o ID do grupo SSO no local storage
	 * @param atrIdGrupoSSO ID do grupo SSO
	 */
	public static setSso(atrIdGrupoSSO: number) {
		StorageService.setItem('atrIdGrupoSSO', { value: atrIdGrupoSSO });
	}

	/**
	 * Retorna o ID do grupo SSO do local storage
	 */
	public static getSso(): number {
		return StorageService.getItem('atrIdGrupoSSO').value as number;
	}
}
