import { BaseService } from '../base.service';

export class StorageService extends BaseService {
	public static setItem(key: string, obj: { [key: string]: any }) {
		if (this.isStorage()) {
			return localStorage.setItem(key, JSON.stringify(obj));
		}
	}
	public static getItem(key: string) {
		let storageItems: string | null = null;
		if (this.isStorage()) {
			storageItems = localStorage.getItem(key);
		}
		return storageItems ? JSON.parse(storageItems) : storageItems;
	}
	public static removeItem(key: string) {
		if (this.isStorage()) {
			localStorage.removeItem(key);
		}
	}
	public static getKey(i: string) {
		let index: string | null = null;
		if (this.isStorage()) {
			index = localStorage.getItem(i);
		}
		return index;
	}
	public static clear(): void {
		if (this.isStorage()) {
			localStorage.clear();
		}
	}

	private static isStorage(): boolean {
		return (window.localStorage) ? true : false;
	}
}
