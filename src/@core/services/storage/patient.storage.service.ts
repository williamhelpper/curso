import { StorageService } from './storage.service';

export class PatientStorageService {

	/**
	 * Armazena o ID do grupo SSO no local storage
	 * @param atrIdGrupoSSO ID do grupo SSO
	 */
	public static setPatient(patient: any) {
		StorageService.setItem('patientData', { value: patient });
	}

	/**
	 * Retorna o ID do grupo SSO do local storage
	 */
	public static getPatient(): any {
		return (StorageService.getItem('patientData') || { value: undefined }).value;
	}
}
