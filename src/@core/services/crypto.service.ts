import CryptoJS from '../../assets/scripts/crypto-js';

/**
 * Faz a de/codificação de textos
 */
export class CryptoService {

	public static encrypt(data: string) {
		return CryptoJS.AES.encrypt(data, this.password).toString();
	}

	public static decrypt(encryptedMessage: string) {
		const bytes = CryptoJS.AES.decrypt(encryptedMessage.toString(), this.password);
		return bytes.toString(CryptoJS.enc.Utf8);
	}

	private static password = '#20_the_secret_pass_populis_19#';
}
