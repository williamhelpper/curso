import { languages } from '../../assets/translate/languages';
import { StorageService } from './storage/storage.service';

interface IUserNavigator extends Navigator {
	userLanguage: string;
}

export class LanguageService {

	static language: string = 'pt';

	static setLanguage() {
		let language: string;
		language = navigator.language || (navigator as IUserNavigator).userLanguage;
		const storageLanguage = (JSON.parse(localStorage.getItem('language')) as any) && (JSON.parse(localStorage.getItem('language')) as any).language;
		storageLanguage && (language = storageLanguage);
		if (LanguageService.language === language) { return; }
		if (language.startsWith('pt')) { LanguageService.language = 'pt'; }
		if (language.startsWith('en')) { LanguageService.language = 'en'; }
		if (language.startsWith('es')) { LanguageService.language = 'es'; }
	}

	static changeLanguage(language: 'pt' | 'es' | 'en') {
		StorageService.setItem('language', { language });
		window.location.reload();
	}

	static translate(text: string): string {
		if (!text) { return; }
		text = text.trim();
		return languages[LanguageService.language] && languages[LanguageService.language][text]
			? languages[LanguageService.language][text]
			: text;
	}
}
