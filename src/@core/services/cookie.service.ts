
export class CookieService {
	public static setCookie(key: string, value: string | number, daysToExpire: number = 365, originPath: string = '/') {
		const d = new Date();
		d.setTime(d.getTime() + (daysToExpire * 24 * 60 * 60 * 1000));
		const expires = 'expires=' + d.toUTCString();
		document.cookie = key + '=' + value + ';' + expires + ';path=' + originPath;
	}
	public static getCookie(key: string) {
		const name = key + '=';
		const splitItensCookies = document.cookie.split(';');
		// tslint:disable-next-line: prefer-for-of
		for (let i = 0; i < splitItensCookies.length; i++) {
			let c = splitItensCookies[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}
	public static showCookie() {
		return document.cookie;
	}
	public static deleteCookie() {
		const arrItensCookie = document.cookie.split(';');
		arrItensCookie.forEach((item: string) => {
			const key = item.split('=')[0].trim();
			document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
		});
	}
}
