// https://gist.github.com/dmnsgn/36b26dfcd7695d02de77f5342b0979c7
// https://gist.github.com/tkafka/1c5174ed5b446de7dfa4c04a3b09c95f

export class Events {
	public static listen() {
		const listeners =
			(function listAllEventListeners() {
				const allElements = Array.prototype.slice.call(document.querySelectorAll('*'));
				allElements.push(document); // we also want document events
				const types = [];
				for (const ev in window) {
					if (/^on/.test(ev)) { types[types.length] = ev; }
				}

				const elements = [];
				// tslint:disable-next-line: prefer-for-of
				for (let i = 0; i < allElements.length; i++) {
					const currentElement = allElements[i];
					// tslint:disable-next-line: prefer-for-of
					for (let j = 0; j < types.length; j++) {
						if (typeof currentElement[types[j]] === 'function') {
							elements.push({
								node: currentElement,
								type: types[j],
								// tslint:disable-next-line: object-literal-sort-keys
								func: currentElement[types[j]].toString(),
							});
						}
					}
				}

				return elements.sort(function(a, b) {
					return a.type.localeCompare(b.type);
				});
			})();
		return listeners;
	}
}
