import { BaseService } from '../common/service.core';

/**
 * Classe para manipulação de tamanhos/posições de elementos de acordo com o viewport ou o offsetParent.
 * @author Felipe Gabriel
 * @since 12/2019
 */
export class WindowService extends BaseService {

	/**
	 * Returns the instance of the class
	 */
	static getInstance(): WindowService {

		if (!WindowService.instance) {
			WindowService.instance = new WindowService();
		}

		return WindowService.instance;
	}

		private static instance: WindowService;

	private constructor() { super(); }

		/**
     * Retorna os valores de offset do elemento
     * @param element Elemento do qual será retornado as posições
     */
		public getElementOffset(element: HTMLElement | Element): ClientRect {
				return (element.getBoundingClientRect() as ClientRect);
		}

		/**
     * Reajusta o tamanho do elemento para não  quebrar o seu wrapper de acordo com seu offset parent
     *
     * Deve ser utilizado em uma atribuição ao estilo marginLeft do componente, exemplo:
     *
     * ```ts
     * this.style.marginLeft = this.windowService.resizeElementWithoutMargin(this);
     * ```
     * @param element Elemento que terá o tamanho reajustado
     */
		// public resizeElementWithoutMargin(element: HTMLElement): string {
		//     const offset = this.getElementOffset(element);
		//     const parentOffset = this.getElementOffset(element.offsetParent);
		//     if (offset.right > parentOffset.width) {
		//         return `${parentOffset.width}px`;
		//     }
		//     return 'none';
		// }

		/**
     * Reajusta o tamanho do elemento para não quebrar o seu wrapper de acordo com seu offset parent e margem
     * @param element Elemento que terá o tamanho reajustado
     */
		// public resizeElementWithMargin(element: HTMLElement): string {
		//     const offset = this.getElementOffset(element);
		//     const parentOffset = this.getElementOffset(element.offsetParent);
		//     if (offset.right > parentOffset.width) {
		//         const margin = offset.right - offset.width;
		//         return `${parentOffset.width - margin}px`;
		//     }
		//     return 'none';
		// }

		/**
     * Retorna se necessário uma margin-left negativa para um elemento se ele quebrar o innerWidth da Window.
     * Deve ser utilizado em uma atribuição ao estilo marginLeft do componente, exemplo:
     *
     * ```ts
     * this.style.marginLeft = this.windowService.adjustPosition(this);
     * ```
     * @param element Elemento sob qual será feita a verificação
     */
		public adjustPosition(element: HTMLElement): string {
				let marginLeft: number = 0;
				const offset = this.getElementOffset(element);

				if (offset.right > window.innerWidth) {
						marginLeft = offset.right - window.innerWidth + 25;
				}
				return `${-marginLeft}px`;
		}

		/**
     * Retorna se necessário uma margin-left negativa para um elemento ser centralizado de acordo com o innerWidth da Window.
     * Deve ser utilizado em uma atribuição ao estilo marginLeft do componente, exemplo:
     *
     * ```ts
     * this.style.marginLeft = this.windowService.getCentralizedMarginLeft(this);
     * ```
     * @param element Elemento sob qual será feita a verificação
     */
		public getCentralizedMarginLeft(element: HTMLElement): string {
				let marginLeft: number = 0;
				const offset = this.getElementOffset(element);
				const widthDifference = window.innerWidth - offset.width;

				if (widthDifference > 0) {
						marginLeft = offset.left - (widthDifference / 2);
				}
				return `${-marginLeft}px`;
		}
}
