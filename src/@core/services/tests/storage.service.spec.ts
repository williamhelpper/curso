import { expect } from '@open-wc/testing';
import { StorageService } from '../storage/storage.service.js';

describe('describe tests in StorageService', () => {

	it('[StorageService] should store data', async () => {
		StorageService.setItem('KarmaTest', { value: 'test value' });
		expect(StorageService.getItem('KarmaTest').value).to.deep.equal('test value');
	});

	it('[StorageService] should override data', async () => {
		StorageService.setItem('KarmaOverride', { value: 'test value' });
		StorageService.setItem('KarmaOverride', { value: 'override value' });
		expect(StorageService.getItem('KarmaOverride').value).to.deep.equal('override value');
	});

	it('[StorageService] should delete data', async () => {
		StorageService.setItem('KarmaDelete', { value: 'test value' });
		StorageService.setItem('KarmaDelete2', { value: 'test value 2' });
		StorageService.removeItem('KarmaDelete');
		expect(StorageService.getItem('KarmaDelete')).to.be.null;
		StorageService.clear();
		expect(StorageService.getItem('KarmaDelete2')).to.be.null;
	});

});
