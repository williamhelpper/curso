import { expect } from '@open-wc/testing';
import { CookieService } from '../../../@core/services/cookie.service.js';

describe('describe tests in CookieService', () => {

		it('[CookieService] should store cookies', async () => {
				CookieService.setCookie('KarmaCookie', 'test value', 1);
				expect(CookieService.getCookie('KarmaCookie')).to.deep.equal('test value');
		});

		it('[CookieService] should override cookies', async () => {
				CookieService.setCookie('KarmaCookieOverride', 'test value', 1);
				CookieService.setCookie('KarmaCookieOverride', 'override value', 1);
				expect(CookieService.getCookie('KarmaCookieOverride')).to.deep.equal('override value');
		});

		it('[CookieService] should delete cookies', async () => {
				CookieService.setCookie('KarmaCookieDelete', 'test value', 1);
				CookieService.deleteCookie();
				expect(CookieService.getCookie('KarmaCookieDelete')).to.equal('');
		});

});
