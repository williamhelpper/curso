import { expect } from '@open-wc/testing';
import { CryptoService } from '../../../@core/services/crypto.service.js';

let encrypt: boolean;
const password: string = 'Karma123';

describe('describe tests in CryptoService', () => {

		it('[CryptoService] should encrypt', async function() {
				expect(password, `Didn't encrypt value`).to.not.equal(CryptoService.encrypt(password));
				expect(CryptoService.encrypt(password), `Encrypted value is invalid`).to.be.ok;
				encrypt = true;
		});

		it('[CryptoService] should decrypt', async function() {
				if (!encrypt) { this.skip(); }

				expect(password, `Didn't decrypt value`).to.deep.equal(CryptoService.decrypt(CryptoService.encrypt(password)));
		});

});
