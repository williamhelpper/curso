
/**
 * Retorna uma string sem caracteres especiais
 * @param string { string | number}
 */
export function clearSpecialcharacters(string: string | number) {
	return string.toString().replace(/[^a-zA-Z0-9]/g, '');
}

export function base64enconde(encodedData) {
	return window.btoa(encodedData);
}

export function base64decode(decodedData) {
	return window.atob(decodedData);
}
