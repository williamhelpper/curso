
import { BehaviorSubject, Observable } from 'rxjs';
import { LanguageService } from './language.service';

export interface IAlert {
	visible: boolean;
	confirm?: boolean;
	response?: boolean;
	message: string;
}

/**
 * AlertService
 * Service responsável pela exibição dos modais de Alert do Sistema
 * ```ts
 * // Exemplo
 * open(){ AlertService.getInstance().show('Abrir esse lindo alert', true); }
 * onInit() { AlertService.getInstance().getObservable().subscribe(data => console.log(data)) }
 * ```
 * @autor William Oliveira
 * @since 11/2019
 */
export class AlertService {

	/**
	 * Returns the instance of the class
	 */
	static getInstance(): AlertService {

		if (!AlertService.instance) {
			AlertService.instance = new AlertService();
		}

		return AlertService.instance;
	}

	private static instance: AlertService;

	private alert: BehaviorSubject<IAlert> = new BehaviorSubject({ visible: false, message: '' });

	private constructor() { /* Services - cannot be instantiated by the 'constructor' */ }
	/**
	* show
	* Exibe os modais de Alert do Sistema
	*
	* @param message: string
	* @param confirm?: boolean parametro para adicionar um botão de confirmação
	* @return void
	* @autor William Oliveira
	* @since 11/2019
	*/
	public show(message: string, confirm?: boolean): void {
		this.alert.next({ visible: true, message: LanguageService.translate(message), confirm });
	}
	/**
	* hide
	* Oculta modais de Alert do Sistema
	*
	* @param response Ao ocultar o modal, passa-se o response, que é um boolean referente, se o modal foi fechado ao clicar em Confirmar,
	* response: true, ou em fechar ou no overlay response:false, de forma a identificar o origem do fechamento do modal
	* @return void
	* @autor William Oliveira
	* @since 11/2019
	*/
	public hide(response: boolean): void {
		this.alert.next({ visible: false, message: '', response });
	}
	/**
	* getObservable
	* Retorna um observable para monitorar o estado do modal
	*
	* @return Observable<IAlert>
	* @autor William Oliveira
	* @since 11/2019
	*/
	public getObservable(): Observable<IAlert> {
		return this.alert.asObservable();
	}

}
