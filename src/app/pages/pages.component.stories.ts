import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { PagesComponent } from './pages.component';

storiesOf('pages-component', module)
	.addDecorator(withKnobs)
	.add('pages-component', () => withClassPropertiesKnobs(PagesComponent))
	.add('pages-component', () => html`<pages-component></pages-component>`);