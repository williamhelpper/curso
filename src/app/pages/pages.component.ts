import { html, customElement, property, css } from 'lit-element';
import { BaseComponent } from '../../@core/common/component.core';
import { Router } from '@vaadin/router';

@customElement('pages-component')
export class PagesComponent extends BaseComponent {

	onAfterViewInit() {
		const container = this.shadowRoot.getElementById('outlet');
		const router = new Router(container);
		const routes: Router.Route[] = [
			{ path: '/', component: 'list-component' },
			{ path: '/detail/(.*)', component: 'detail-component' },
			{ path: '/form(.*)', component: 'form-component' },
		];
		router.setRoutes(routes);
	}

	render() {
		return html`
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!-- render routes -->
						<div id="outlet"></div>
					</div>
				</div>
			</div>
		`;
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
