import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { DetailComponent } from './detail.component';

storiesOf('detail-component', module)
	.addDecorator(withKnobs)
	.add('detail-component', () => withClassPropertiesKnobs(DetailComponent))
	.add('detail-component', () => html`<detail-component></detail-component>`);