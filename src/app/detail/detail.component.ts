import { html, customElement, property, css } from 'lit-element';
import { BaseComponent, Location } from '../../@core/common/component.core';
import { ProductsApi } from '../../@core/api/products.api';

@customElement('detail-component')
export class DetailComponent extends BaseComponent {

	// @property() foo: string = 'bar';

	private detail: any;

	onInit() {
		const params = Location.getParams();
		console.log('params: ', params);
		this.getItem(params.id);
	}

	getItem(id) {
		ProductsApi.get({ id }).then(product => {
			this.detail = product.data[0];
			this.forceUpdate();
		});
	}

	render() {
		if (this.detail){
		return html`
			<div class="card mb-4 shadow-sm">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">${this.detail.plano}</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">$${this.detail.valor} <small class="text-muted">/ mês</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
					<li>${this.detail.usuarios} usuários</li>
					<li>${this.detail.armazenamento} GB de armazenamento</li>
					<li>${this.detail.suporte}</li>
					<li>Acesso ao centro de ajuda</li>
					</ul>
					<button type="button" class="btn btn-lg btn-block btn-outline-primary">Cadastre-se de graça</button>
				</div>
			</div>
		`;
		}else{
			return html``;
		}
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
