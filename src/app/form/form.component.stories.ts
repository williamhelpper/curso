import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { FormComponent } from './form.component';

storiesOf('form-component', module)
	.addDecorator(withKnobs)
	.add('form-component', () => withClassPropertiesKnobs(FormComponent))
	.add('form-component', () => html`<form-component></form-component>`);