import { html, customElement, property, css } from 'lit-element';
import { BaseComponent } from '../../@core/common/component.core';
import { ProductsApi } from '../../@core/api/products.api';

@customElement('form-component')
export class FormComponent extends BaseComponent {

	@property() foo: string = 'bar';

	private detalhes: any = {};

	render() {
		return html`
			<form>
				<input-component label="Numero usuários"
					@input-bind=${(event) => this.detalhes.usuarios = event.detail}></input-component>
				<input-component label="Plano"
					@input-bind=${(event) => this.detalhes.plano = event.detail}></input-component>
				<input-component label="Valor"
					@input-bind=${(event) => this.detalhes.valor = event.detail}></input-component>
				<input-component label="Suporte"
					@input-bind=${(event) => this.detalhes.suporte = event.detail}></input-component>
				<button-component label="Salvar" btnClass="primary" @click=${() => this.create(this.detalhes)}></button-component>
			</form>
		`;
	}

	private create(detalhes): void {
		ProductsApi.create(detalhes).then(response => console.log(response));
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
