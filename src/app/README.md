# App

## Módulos

As páginas e os componentes estão sendo exportados em seus respectivos módulos:

**pages.module.ts**

```ts
export { HomePageComponent } from './home-page/home-page.component';
export { LoginPageComponent } from './login-page/login-page.component';
...

export class PagesModule { }
```

`index.ts` dentro de app está importando esses módulos, logo, eles estão disponíveis para todas as páginas, sem necessidade de importar novamente:

**index.ts**

```ts
import { PagesModule } from './pages/pages.module';
import { ComponentsModule } from './components/components.module';
...

export { PagesModule, ComponentsModule };
```

