import { html, customElement, property, css } from 'lit-element';
import { BaseComponent } from '../../../@core/common/component.core';

@customElement('button-component')
export class ButtonComponent extends BaseComponent {

	@property({ type: String }) label: string;
	@property({ type: String }) btnClass: string;

	render() {
		return html`
			<button type="button" class="btn btn-${this.btnClass}">X ${this.label}</button>
		`;
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
