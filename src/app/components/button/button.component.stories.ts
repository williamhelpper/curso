import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { ButtonComponent } from './button.component';

storiesOf('button-component', module)
	.addDecorator(withKnobs)
	.add('button-component', () => withClassPropertiesKnobs(ButtonComponent))
	.add('button-component', () => html`<button-component></button-component>`);