import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { InputComponent } from './input.component';

storiesOf('input-component', module)
	.addDecorator(withKnobs)
	.add('input-component', () => withClassPropertiesKnobs(InputComponent))
	.add('input-component', () => html`<input-component></input-component>`);