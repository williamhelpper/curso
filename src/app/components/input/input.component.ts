import { html, customElement, property, css } from 'lit-element';
import { BaseComponent } from '../../../@core/common/component.core';

@customElement('input-component')
export class InputComponent extends BaseComponent {

	@property({type: String}) label: string;

	render() {
		return html`
			<div class="form-group">
				<label for="input-usuarios">${this.label}</label>
				<input @keyup=${(event) => { this.emitValue(event.path[0].value) }} 
					type="text" class="form-control" id="input-usuarios" placeholder="Entre com numero usuarios">
			</div>
		`;
	}

	private emitValue(value: string): void {
		this.dispatchEvent(new CustomEvent('input-bind', { detail: value }));
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
