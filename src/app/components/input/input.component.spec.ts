import { fixture, expect } from '@open-wc/testing';
import './input.component.js';

describe('describe tests in input', function() {

	it('can instantiate an element', async function() {
		const el = await fixture('<input-component></input-component>');
		expect(1).to.equal(1);
	})

})