import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { PagesComponent } from './pages/pages.component';
import { ButtonComponent } from './components/button/button.component';
import { FormComponent } from './form/form.component';
import { InputComponent } from './components/input/input.component';
export {
	PagesComponent,
	ListComponent,
	DetailComponent,
	ButtonComponent,
	FormComponent,
	InputComponent,
};

import { Router } from '@vaadin/router';

const container = document.getElementById('outlet');
const routers: Router.Route[] = [
	{
		path: '/(.*)',
		component: 'pages-component',
	}
];
const route = new Router(container);
route.setRoutes(routers);
