import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { ListComponent } from './list.component';

storiesOf('list-component', module)
	.addDecorator(withKnobs)
	.add('list-component', () => withClassPropertiesKnobs(ListComponent))
	.add('list-component', () => html`<list-component></list-component>`);