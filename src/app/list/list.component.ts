import { html, customElement, property, css } from 'lit-element';
import { BaseComponent, Location } from '../../@core/common/component.core';
import { ProductsApi } from '../../@core/api/products.api';

@customElement('list-component')
export class ListComponent extends BaseComponent {

	private arrayProducts = []

	onInit() {
		this.list();
	}

	list() {
		ProductsApi.list().then(products => {
			console.log('products.data: ', products.data);
			this.arrayProducts = products.data;
			this.forceUpdate();
		}).catch(error => console.log(error));
	}

	delete(id) {
		ProductsApi.delete(id).then(() => {
			alert('Item deletado com sucesso. ');
			this.list();
		}).catch(error => console.log(error));
	}

	detail(id) {
		Location.goPage('detail/', { id });
	}

	template(){
		return this.arrayProducts.length == 0 ? html`Não existem items` :
		this.arrayProducts.map(item => html`<tr>
			<th scope="row">${item.id}</th>
			<td>${item.usuarios}</td>
			<td>${item.plano}</td>
			<td>${item.valor}</td>
			<td>${item.suporte}</td>
			<!-- <td><button-component @click=${evt => this.delete(item.id)} btnClass="primary" label="custom button"></button-component></td> -->
			<td><button type="button" @click=${evt => this.delete(item.id)} class="btn btn-danger">X Remove</button></td>
			<td><button type="button" @click=${evt => this.detail(item.id)} class="btn btn-primary">V Detalhe</button></td>
		</tr>`);
	}

	render() {
		return html`
			<table class="table">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">usuario</th>
					<th scope="col">plano</th>
					<th scope="col">valor</th>
					<th scope="col">suporte</th>
					<th scope="col">actions</th>
					</tr>
				</thead>
				<tbody>
					${this.template()}
				</tbody>
			</table>
		`;
	}

}
