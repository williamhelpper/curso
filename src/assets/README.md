# Assets

Aqui devem ir os recursos gráficos utilizados no sistema.

## CSS

Os arquivos de CSS são gerados automaticamente pelo start/build. Não devem ser feitas alterações neles aqui.

## Fonts

Aqui devem ser inclusas as fontes que serão utilizadas no sistema, por padrão Open Sans e seus diferentes pesos de fonte (300, 400 e 600).

## Images

Aqui devem ser inclusas as imagens que serão utilizadas no sistema. Essas imagens serão levadas também no build do projeto.