# @Theme

Aqui devem ir os arquivos .scss do sistema. Esses arquivos serão transpilados para .css no momento de start/build.

## Bootstrap

No arquivo **bootstrap.scss**, devem ser importados somente os recursos que forem ser utilizados no sistema. Os outros recursos devem ser mantidos como comentados:

```scss
// será utilizado
@import "../../node_modules/bootstrap/scss/navbar";

// não será utlizado
// @import "../../node_modules/bootstrap/scss/jumbotron";
```