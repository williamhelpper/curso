import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';
import { terser } from "rollup-plugin-terser"; /* https://github.com/TrySound/rollup-plugin-terser */
import copy from 'rollup-plugin-copy'; /* https://github.com/vladshcherbin/rollup-plugin-copy */
import replace from 'rollup-plugin-replace'; /* alterar modo production ou development */

export default {
	input: 'src/index.ts',
	output:
	{
		format: "umd",
		file: 'dist/index.umd.js',
		sourcemap: false,
		name: 'my_bundle',
	},
	plugins: [
		replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
		typescript(
			{
				tsconfig: "tsconfig.json",
				useTsconfigDeclarationDir: true,
				objectHashIgnoreUnknownHack: true,
			}
		),
		nodeResolve(),
		commonjs({
			namedExports: {
				'node_modules/moment/moment.js': ['moment']
			}
		}),
		terser(),
		copy({
			targets: [
				{ src: 'src/assets/fonts/*', dest: 'dist/assets/fonts' },
				{ src: 'src/assets/css/*', dest: 'dist/assets/css' },
				{ src: 'src/assets/scripts/*', dest: 'dist/assets/scripts' },
				{ src: 'src/index.html', dest: 'dist' },
				{ src: 'src/assets/images/*', dest: 'dist/assets/images' },
				{ src: 'src/assets/populis.ico', dest: 'dist/assets' },
			]
		}),
	]
};