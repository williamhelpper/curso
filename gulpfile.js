/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const exec = require('gulp-exec');
const concat = require('gulp-concat'); // https://www.npmjs.com/package/gulp-concat
const sourcemaps = require('gulp-sourcemaps');
const strip = require('gulp-strip-comments');
const wait = require('gulp-wait');
// const replace = require('gulp-replace');
// const del = require('del');
// const spawn = require('child_process').spawn;


gulp.task('sass-compile', () => {
	return gulp.src(['./src/**/*.scss'])
		// return gulp.src(['./src/@theme/**/*.scss', './src/**/*.scss', './src/app/**/*.scss'])
		.pipe(sass({
			outputStyle: 'nested'
		}).on('error', sass.logError))
		.pipe(wait(200))
		.pipe(concat('all.css'))
		.pipe(rename('global.min.css'))
		.pipe(gulp.dest('./src/assets/css'))
		.pipe(strip.text())
		.pipe(exec('npm run update:global_style', { continueOnError: false, pipeStdout: false, customTemplatingThing: "test" }))
});

gulp.task('sass-compile-with-map', () => {
	return gulp.src(['./src/@theme/**/*.scss', './src/**/*.scss'])
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(sourcemaps.init())
		.pipe(concat('global.css'))
		.pipe(sourcemaps.write())
		.pipe(rename('global.min.css'))
		.pipe(strip.text())
		.pipe(gulp.dest('./src/assets/css'))
		.pipe(exec('npm run update:global_style', { continueOnError: false, pipeStdout: false, customTemplatingThing: "test" }))
});

gulp.task('exec', function (evt) {
	return gulp.src('./**/**')
		.pipe(exec('npm run update:global_style', { continueOnError: false, pipeStdout: false, customTemplatingThing: "test" }))
})

// Task 'watch' - Run with command 'gulp watch'
gulp.task('watch', function () {
	gulp.watch('./src/**/*.scss', gulp.series(['sass-compile']));
});

gulp.task('default', gulp.parallel('sass-compile', 'watch'));
