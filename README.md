# Populis - System

# Git Flow

## Branches

### Master

Sem conteúdo, somente readme.

### Production

Contem o conteúdo referente a versão mais atualizada e em produção.

### Development

Contem o conteúdo em desenvolvimento, novas funcionalidades, testes, correções, etc.

### Readme

Contém commits referentes aos diferentes arquivos `README.md` do projeto.

### Release

Pré-produção, é o merge de development com production, onde são feitos testes e a correção dos conflitos e bugs. Após tudo correto aqui, é feito o merge com production.

### Features

Para novas funcionalidades, são criadas branchs com prefixo *feature*. 

Exemplo: 
```shell
git checkout -b feature/component/list
```

Após a feature ser desenvolvida, faça o merge com development.

Exemplo:

* entre em development

```shell
git checkout development
```

* faça o merge de feature/component/list para development

```shell
git merge feature/component/list
```

* após a branch ser mergeada e entregue, ela será excluida

```shell
git branch -d feature/component/list
```

## Commits

### Release

Commits na branch *release* devem conter mensagens com o seguinte padrão: `release - 1.0.0 - description`. A descrição deve ser escrita em inglês, para condizer com o Git Flow. Exemplo:

```shell
git commit -m "release - 1.6.7 - create user-group module"
```

### Features

Commits em branches *feature* devem conter mensagens com o seguinte padrão: `feature - o que foi feito`. As mensagens devem ser escritas em inglês, para condizer com o Git Flow. Não é necessário informar o escopo da branch na mensagem (como em qual componente ou service condiz o commit), pois isso já é definido no nome da mesma. Entregas parciais devem conter `in progress` no final. Exemplo:

```shell
git commit -m "feature - create component, in progress"
```