import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';
import dev from 'rollup-plugin-dev';
import html from 'rollup-plugin-html';
import livereload from 'rollup-plugin-livereload';
import replace from 'rollup-plugin-replace'; // alterar modo production ou development
// import visualizer from 'rollup-plugin-visualizer';

export default {
	input: 'src/index.ts',
	watch: {
		chokidar: false
	},
	output: [
		{
			format: "umd",
			file: 'dist/index.umd.js',
			sourcemap: false,
			name: 'my_bundle',
		},
	],
	plugins: [
		typescript(
			{
				tsconfig: "tsconfig.json",
				useTsconfigDeclarationDir: true,
				objectHashIgnoreUnknownHack: true,
			}
		),
		html({
			include: '**/*.html',
			htmlMinifierOptions: {
				collapseWhitespace: true,
				collapseBooleanAttributes: true,
				conservativeCollapse: true,
				minifyJS: true
			}
		}),
		nodeResolve(),
		dev(
			{
				dirs: ['dist', 'src'],
				spa: 'src/index.html',
				port: 3000,
				force: true
			}
		),
		livereload({
			watch: 'dist',
		}),
		commonjs({
			namedExports: {
				'node_modules/moment/moment.js': ['moment'],
				'./src/assets/scripts/crypto-js/crypto-js': ['CryptoJS'],
				'./src/assets/scripts/pikaday/pikaday': ['pikaday']
			}
		}),
		replace({
			'process.env.NODE_ENV': JSON.stringify('production')
		}),
		// visualizer({
		// 	filename: 'dist/project-dependencies-treemap.html',
		// 	title: 'Todas as dependências do projeto',
		// 	template: 'treemap'
		// }),
		// visualizer({
		// 	filename: 'dist/project-dependencies-circlepacking.html',
		// 	title: 'Todas as dependências do projeto',
		// 	template: 'circlepacking'
		// })
	]
};
