import { configure, addDecorator } from '@storybook/polymer';
import { withA11y } from '@storybook/addon-a11y';
import '@storybook/addon-console';

import '../src/assets/css/global.min.css';

// recupera os arquivos para criar o storybook
// const req = require.context('../src', true, /\.stories\.js$/);  // pasta de onde irá recuperar os arquivos para criar o storybook
const req = require.context('../dist.storybook', true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

addDecorator(withA11y);
configure(loadStories, module);
