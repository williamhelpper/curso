import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';
import html from 'rollup-plugin-html';

const pathFolder = 'dist.test';

export default {
	input: 'src/index.ts',
	output: [
		{
			format: "umd",
			file: pathFolder + '/index.umd.js',
			sourcemap: false,
			name: 'my_bundle',
		},
	],
	plugins: [
		typescript(
			{
				tsconfig: "tsconfig.test.json",
				useTsconfigDeclarationDir: true,
				objectHashIgnoreUnknownHack: true,
			}
		),
		html({
			include: '**/*.html',
			htmlMinifierOptions: {
				collapseWhitespace: true,
				collapseBooleanAttributes: true,
				conservativeCollapse: true,
				minifyJS: true
			}
		}),
		nodeResolve(),
		commonjs({
			namedExports: {
				'node_modules/moment/moment.js': ['moment']
			}
		}),
		copy({
			targets: [
				{ src: 'src/assets/scripts/*', dest: pathFolder + '/assets/scripts' },
				{ src: 'src/index.html', dest: pathFolder },
				{ src: 'src/assets/images/*', dest: pathFolder + '/assets/images' },
			]
		}),
	]
};