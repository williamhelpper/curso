const { exec } = require('child_process');



module.exports = {
    logError: logError,
    logAction: logAction,
    logTitle: logTitle,
    answer: answer,
    command: command
}



function logError(...args) {
    const color_log = '\n\x1b[1m\x1b[41m\x1b[37m ERROR: \x1b[0m \x1b[1m\x1b[31m%s\x1b[0m\n';
    console.error(color_log, ...args);
    process.exit(1);
}

function logAction(action, value) {
    const color_log = '\x1b[36m%s: \x1b[32m%s\x1b[0m';
    console.log(color_log, action, value);
}

function logTitle(title) {
    const color_log = '\n\x1b[36m\x1b[1m%s\x1b[0m';
    console.log(color_log, title);
}


async function command(command) {

    logTitle('Executando comando: \x1b[33m' + command);

    return new Promise(resolve => {
        exec(command, (err, stdout, stderr) => {
            if(err || stderr) {
                logError('Erro ao executar o comando', err || stderr);
            }

            console.log(stdout + '\n');
            resolve(true);
        });
    });
}


async function answer(title) {
    return new Promise(resolve => {
        const color = '\x1b[1m\x1b[35m%s\x1b[0m';
        process.stdout.write(str_format(color, '\n'+ title +' '));
        process.stdin.on('readable', () => {
            console.log(''); // Quebra uma linha
            resolve(String(process.stdin.read()).trim());
        });
    });
}

function str_format(...args) {
    return args.shift().replace(/%([jsd])/g, x => x === '%j' ? JSON.stringify(args.shift()) : args.shift());
}
