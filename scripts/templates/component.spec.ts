import { fixture, expect } from '@open-wc/testing';
import './{Selector}.component.js';

describe('describe tests in {Selector}', function() {

	it('can instantiate an element', async function() {
		const el = await fixture('<{Selector}-component></{Selector}-component>');
		expect(1).to.equal(1);
	})

})