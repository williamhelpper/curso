import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { {NameClass}Component } from './{Selector}.component';

storiesOf('{Selector}-component', module)
	.addDecorator(withKnobs)
	.add('{Selector}-component', () => withClassPropertiesKnobs({NameClass}Component))
	.add('{Selector}-component', () => html`<{Selector}-component></{Selector}-component>`);