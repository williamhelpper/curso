import { html, customElement, property, css } from 'lit-element';
import { BaseComponent } from '{PathBase}@core/common/component.core';

@customElement('{Selector}-component')
export class {NameClass}Component extends BaseComponent {

	@property() foo: string = 'bar';

	render() {
		return html`
			<div class="container-fluid page">
				<div class="row">
					{NameClass}Component is worked!
				</div>
			</div>
		`;
	}

	static get styles() {
		return [super.styles, css`

		`]
	}
}
