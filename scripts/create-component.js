const fs = require('fs');
const path = require('path');
const utils = require('./utils');

let nameComponent = '';
let pathComponent = './src/app/components';

async function init() {

	utils.logTitle(`
		1 parameter is the name of the component and folder, ex. 'email'
		2 parameter is optional, is the component path, 
		Example './src/app/components/input'
		By default components are created inside the 'src/app/components' folder.
	`);

	/* 1 param [name] */ (process.argv[2]) ? (nameComponent = process.argv[2]) : utils.logError('Necessary to pass the name of the component to be created as a parameter.');
	/* 2 param [path] */ (process.argv[3]) && (pathComponent = process.argv[3]);

	await createFolder(nameComponent, pathComponent);
	await writeFiles(nameComponent, pathComponent);

}


init();

async function createFolder(name, pathFolder) {
	folderPath = path.resolve(__dirname, '..', pathFolder, name);
	if (!fs.existsSync(folderPath)) {
		await fs.mkdirSync(folderPath, { recursive: true }, err => { utils.logError(err); });
		utils.logAction('CREATE', 'Successfully created component');
	} else {
		utils.logError('You hear an error creating the component, possibly this component already exists.')
	}
}

async function createFile(name = 'component', content = 'text', extension = 'ts', folderPath) {
	const filePath = path.resolve(__dirname, '..', folderPath, name, `${name}.component.${extension}`);
	fs.writeFile(filePath, content, function (err) {
		if (err) {
			utils.logError(`an error occurred while creating the file: ${filePath}`)
			throw err;
		}
		utils.logAction('CREATE', `File is created successfully: ${filePath}`);
	});
}

function getContentTemplateFile(templateFile, replaceNameFile) {
	const templatePath = path.resolve(__dirname, 'templates', templateFile);
	return new Promise(resolve => {
		fs.readFile(templatePath, 'utf8', (err, data) => {
			if (err) utils.logError('Error reading template', err);
			resolve(replaceTemplate(data, replaceNameFile));
		});
	})
}

function replaceTemplate(templateContent, textReplace) {
	textReplace = textReplace.toString().toLowerCase();
	const nameClass = textReplace.split('-').map(name => name[0].toUpperCase() + name.substring(1, name.length)).join('');
	const pathBase = pathComponent.replace(/^.\//g, '').split('/').map(_ => '../').join('');
	return templateContent.toString()
		.replace(/\{PathBase\}/g, pathBase)
		.replace(/\{NameClass\}/g, nameClass)
		.replace(/\{Selector\}/g, textReplace)
}

function writeFiles(nameComponent, pathComponent) {
	const templates = ['component.ts', 'component.stories.ts', 'component.spec.ts'];
	templates.forEach(async (template) => {
		const getContentTemplate = await getContentTemplateFile(template, nameComponent);
		let extension = template.split('.');
		extension = extension.length > 2 ? `${extension[1]}.${extension[2]}` : extension[1];
		createFile(nameComponent, getContentTemplate, extension, pathComponent);
	});
}


