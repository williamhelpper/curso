const fs = require('fs');
const path = require('path');
const utils = require('./utils');


const pathFileCss = path.resolve(__dirname, '..', 'src/assets/css/global.min.css');
const pathGlobalCss = path.resolve(__dirname, '..', 'src/assets/css/global-css.ts');
const pathTemplateGlobalCss = path.resolve(__dirname, 'templates/global.ts');


async function createFile(content) {
	fs.writeFile(pathGlobalCss, content, function (err) {
		if (err) {
			utils.logError(`an error occurred while creating the file: ${pathGlobalCss}`)
			throw err;
		}
		utils.logAction('CREATE', `File is created successfully: ${pathGlobalCss}`);
	});
}

async function init() {
	try {
		const template = await getContentTemplateFile(pathTemplateGlobalCss);
		const content = await getContentTemplateFile(pathFileCss);
		const globalCssContent = template.toString().replace(/\{GLOBAL_CSS\}/g, content)
		await createFile(globalCssContent);
		utils.logAction('CREATE', `File is created successfully: ${pathGlobalCss}`);
	} catch (err) {
		utils.logError('An error occurred while creating the global customization file', err);
	}
}

function getContentTemplateFile(path) {
	return new Promise(resolve => {
		fs.readFile(path, 'utf8', (err, data) => {
			if (err) utils.logError('Error reading template', path);
			resolve(data);
		});
	})
}

init();

