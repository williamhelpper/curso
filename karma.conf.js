/* eslint-disable import/no-extraneous-dependencies */
const { createDefaultConfig } = require('@open-wc/testing-karma');
const merge = require('webpack-merge');

module.exports = config => {
	config.set(
		merge(createDefaultConfig(config), {
			files: [
				'./scripts/window-process.js',
				// runs all files ending with .test in the test folder,
				// can be overwritten by passing a --grep flag. examples:
				//
				// npm run test -- --grep test/foo/bar.test.js
				// npm run test -- --grep test/bar/*
				{ pattern: config.grep ? config.grep : './dist.test/**/**/*.spec.js', type: 'module' },
				// { pattern: config.grep ? config.grep : './src/**/**/*.spec.ts', type: 'module' },
				// { pattern: 'mocks/**/*.html', watched: true, served: true, included: false },
			],

			proxies: {
				'/assets/images/': 'src/assets/images/',
			},

			esm: {
				nodeResolve: true,
			},
			// basePath: '../populis-v3-fe-repo/',
			// basePath: '',
			// frameworks: ['esm'],
			// esm: {
			// 	babel: true,
			// 	// nodeResolve: true,
			// 	// compatibility: 'all',
			// 	// compatibility: 'none',
			// 	fileExtensions: ['.ts', '.js']
			// },

			// you can overwrite/extend the config further
		}),
	);
	return config;
};
